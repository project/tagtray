<?php

/**
 * @file
 * Integrate Druapl with TagTray.
 */

/**
 * Implements hook_element_info().
 */
function tagtray_element_info() {
  return [
    'tagtray' => [
      '#theme' => 'tagtray',
      '#attached' => [
        'library' => [
          ['tagtray', 'tagtray']
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_theme().
 */
function tagtray_theme() {
  return [
    'tagtray' => [
      'variables' => [
        'gallery_code' => FALSE,
        'page_size' => 4,
        'max_num_pages' => 1,
        'layout_type' => 'responsive',
        'grid_options' => 'tagtray-col-xs-12 tagtray-col-sm-3 tagtray-col-md-3 tagtray-col-lg-2',
        'hide_source_icon' => 'true',
      ],
    ],
  ];
}

/**
 * Theme output for the tagtray element.
 */
function theme_tagtray($vars) {
  $defaults = tagtray_theme()['tagtray']['variables'];
  $element = [
    '#theme' => 'html_tag',
    '#tag' => 'div',
    '#attributes' => [
      'class' => ['tagtray-gallery'],
    ],
    '#value' => '',
  ];
  foreach ($defaults as $key => $default) {
    $element['#attributes']['data-' . strtr($key, ['_' => '-'])] = $vars[$key];
  }
  return render($element);
}

/**
 * Implements hook_library().
 */
function tagtray_library() {
  return [
    'tagtray' => [
      'title' => 'TagTray',
      'website' => 'https://www.tagtray.com',
      'version' => '1',
      'js' => [
        '//api.tagtray.com/v2/tagtray.js' => [
          'type' => 'external',
        ],
        '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js' => [
          'type' => 'external',
        ],
      ],
      'css' => [
        '//api.tagtray.com/v2/tagtray-theme-default.css' => [
          'type' => 'external',
        ],
        '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css' => [
          'type' => 'external',
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_block_info().
 */
function tagtray_block_info() {
  $blocks['tagtray_block'] = [
    'info' => t('TagTray'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  ];
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function tagtray_block_configure($delta = '') {
  $form['gallery_code'] = [
    '#type' => 'textfield',
    '#title' => t('Gallery Code'),
    '#default_value' => variable_get('tagtray_block_configuration_gallery_code', ''),
  ];
  return $form;
}

/**
 * Implements hook_block_save().
 */
function tagtray_block_save($delta = '', $edit = array()) {
  variable_set('tagtray_block_configuration_gallery_code', $edit['gallery_code']);
}

/**
 * Implements hook_block_view().
 */
function tagtray_block_view($delta = '') {
  return [
    'content' => [
      '#type' => 'tagtray',
      '#gallery_code' => variable_get('tagtray_block_configuration_gallery_code', ''),
    ],
  ];
}
